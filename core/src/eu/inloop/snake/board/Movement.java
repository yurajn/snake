package eu.inloop.snake.board;

public final class Movement {
    public static final int None = -1;
    public static final int Left = 0;
    public static final int Right = 1;
    public static final int Up = 2;
    public static final int Down = 3;
}
