package eu.inloop.snake.board;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import eu.inloop.snake.common.ResourcePaths;

public class World extends Actor {

    public static final int COUNT_X_TILES = 20, COUNT_X_TILES_HALF = COUNT_X_TILES / 2;
    public static final int COUNT_Y_TILES = 11, COUNT_Y_TILES_HALF = COUNT_Y_TILES / 2;
    public static final int TILE_WIDTH = 40, TILE_WIDTH_HALF = TILE_WIDTH / 2;
    public static final int TILE_HEIGHT = 40, TILE_HEIGHT_HALF = TILE_HEIGHT / 2;
    private static final int MOVEMENT_SPEED_MS = 150;

    private final TiledDrawable mBackground;
    private final TextureAtlas.AtlasRegion mSnakeStart;
    private final TextureAtlas.AtlasRegion mSnakePartG, mSnakePartGF, mSnakePartR, mSnakePartRF;
    private final TextureAtlas.AtlasRegion mAppleGreen, mAppleRed;

    private TileData[][] mTileMap;
    private int mCurrentSnakeMovement;
    private Array<Tile> mSnakeParts;
    private long mLastMovement;
    private SnakeWorldListener mSnakeWorldListener;
    private boolean mPaused, mGameOver;

    private Animation mCracklingAnimation;
    private float mCracklingAnimStateTime;

    public World(AssetManager assetManager, SnakeWorldListener snakeWorldListener) {
        final TextureAtlas textureAtlas = assetManager.get(ResourcePaths.ATLAS_MAIN, TextureAtlas.class);

        mSnakeStart = textureAtlas.findRegion(ResourcePaths.Game.TEXTURE_SNAKE_START);
        mSnakePartG = textureAtlas.findRegion(ResourcePaths.Game.TEXTURE_SNAKE_PART_G);
        mSnakePartGF = textureAtlas.findRegion(ResourcePaths.Game.TEXTURE_SNAKE_PART_GF);
        mSnakePartR = textureAtlas.findRegion(ResourcePaths.Game.TEXTURE_SNAKE_PART_R);
        mSnakePartRF = textureAtlas.findRegion(ResourcePaths.Game.TEXTURE_SNAKE_PART_RF);
        mAppleGreen = textureAtlas.findRegion(ResourcePaths.Game.TEXTURE_APPLE_GREEN);
        mAppleRed = textureAtlas.findRegion(ResourcePaths.Game.TEXTURE_APPLE_RED);
        mBackground = new TiledDrawable(textureAtlas.findRegion(ResourcePaths.Game.TEXTURE_BACKGROUND));
        mCracklingAnimation = new Animation(0.04f, textureAtlas.findRegions(ResourcePaths.Game.ANIMATION_CRACK));

        mSnakeWorldListener = snakeWorldListener;
    }

    public void reset() {
        mSnakeWorldListener.onGameStart();

        //Reset values
        mPaused = false;
        mGameOver = false;
        mCracklingAnimStateTime = 0.0f;
        mCurrentSnakeMovement = Movement.None;

        //Generate tile map
        mTileMap = new TileData[COUNT_Y_TILES][COUNT_X_TILES];
        for (int y = 0; y < COUNT_Y_TILES; y++) {
            for (int x = 0; x < COUNT_X_TILES; x++) {
                mTileMap[y][x] = new TileData(Tile.Type.EMPTY);
            }
        }
        //Create snake in center
        createSnake(1);

        //Generate first apple
        generateApple();
    }

    private void createSnake(int width) {
        mSnakeParts = new Array<Tile>();
        mSnakeParts.add(new Tile(COUNT_X_TILES_HALF, COUNT_Y_TILES_HALF, new TileData(Tile.Type.SNAKE_START)));
        for (int i = 0; i < width; i++) {
            addSnakePart(TileData.COLOR_DEFAULT, true);
        }
    }

    private void addSnakePart(int color) {
        addSnakePart(color, false);
    }

    private void addSnakePart(int color, boolean offsetX) {
        final Tile tile = mSnakeParts.get(mSnakeParts.size - 1);
        final TileData data = new TileData(Tile.Type.SNAKE_PART, color);
        data.extra = tile.data.extra == 0 ? 1 : 0;
        final Tile newTile = new Tile(tile, data);
        newTile.x -= offsetX ? 1 : 0;
        mSnakeParts.add(newTile);
    }

    private TileData getTile(int x, int y) {
        if (x < 0 || y < 0 || x >= COUNT_X_TILES || y >= COUNT_Y_TILES) {
            return new TileData(Tile.Type.OUTSIDE);
        } else {
            return mTileMap[y][x];
        }
    }

    private void setTile(Tile tile) {
        mTileMap[tile.y][tile.x] = new TileData(tile.data);
    }

    private void setTile(Tile tile, int type, int color) {
        mTileMap[tile.y][tile.x].type = type;
        mTileMap[tile.y][tile.x].color = color;
    }

    private void setTile(Tile tile, int type) {
        setTile(tile, type, 0);
    }

    private Array<Tile> getFreeTiles() {
        Array<Tile> emptyTiles = new Array<Tile>();
        for (int y = 0; y < COUNT_Y_TILES; y++) {
            for (int x = 0; x < COUNT_X_TILES; x++) {
                if (getTile(x, y).type == Tile.Type.EMPTY) {
                    emptyTiles.add(new Tile(x, y));
                }
            }
        }
        return emptyTiles;
    }

    private void generateApple() {
        final Array<Tile> freeTiles = getFreeTiles();
        setTile(freeTiles.random(), Tile.Type.APPLE, MathUtils.randomBoolean(0.15f) ? TileData.COLOR_RED : TileData.COLOR_GREEN);
    }

    public void setSnakeMovement(int movement) {
        if (!mPaused) {
            //Ignore wrong opposite movements
            if (mCurrentSnakeMovement == Movement.Up && movement == Movement.Down) {
                return;
            } else if (mCurrentSnakeMovement == Movement.Down && movement == Movement.Up) {
                return;
            } else if (mCurrentSnakeMovement == Movement.Left && movement == Movement.Right) {
                return;
            } else if (mCurrentSnakeMovement == Movement.Right && movement == Movement.Left) {
                return;
            }
            mCurrentSnakeMovement = movement;
        }
    }

    public void moveSnake(int addX, int addY) {
        final Tile headTile = mSnakeParts.get(0);
        final Tile endTile = new Tile(mSnakeParts.get(mSnakeParts.size - 1));

        if (onSnakeMove(getTile(headTile.x + addX, headTile.y + addY))) {
            for (int i = mSnakeParts.size - 1; i >= 0; i--) {
                final Tile part = mSnakeParts.get(i);
                final Tile prevPart = mSnakeParts.get(Math.max(i - 1, 0));

                if (i == 0) {
                    part.x += addX;
                    part.y += addY;
                } else {
                    if (addX != 0 || addY != 0) {
                        part.x = prevPart.x;
                        part.y = prevPart.y;
                    }
                }

                setTile(part);
            }

            if (mCurrentSnakeMovement != Movement.None) {
                setTile(endTile, Tile.Type.EMPTY);
            }
            setTile(headTile, Tile.Type.SNAKE_START);
        } else {
            mPaused = true;
            mGameOver = true;
            mSnakeWorldListener.onGameOver();
        }
    }

    private boolean onSnakeMove(TileData tileData) {
        mSnakeWorldListener.onSnakeMove(tileData.type);

        if (tileData.type == Tile.Type.APPLE) {
            generateApple();
            addSnakePart(tileData.color);
        } else if (tileData.type == Tile.Type.SNAKE_PART || tileData.type == Tile.Type.OUTSIDE) {
            return false;
        }

        return true;
    }

    public Tile getSnakeHeadPosition() {
        return mSnakeParts.first();
    }

    public int getSnakeMovement() {
        return mCurrentSnakeMovement;
    }

    public boolean isGameOver() {
        return mGameOver;
    }

    public void setPause(boolean pause) {
        if (!mGameOver) {
            mPaused = pause;
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (!mPaused) {
            if (TimeUtils.timeSinceMillis(mLastMovement) >= MOVEMENT_SPEED_MS) {
                switch (getSnakeMovement()) {
                    case Movement.Left:
                        moveSnake(-1, 0);
                        break;
                    case Movement.Right:
                        moveSnake(1, 0);
                        break;
                    case Movement.Down:
                        moveSnake(0, -1);
                        break;
                    case Movement.Up:
                        moveSnake(0, 1);
                        break;
                    case Movement.None:
                        moveSnake(0, 0);
                        break;
                }
                mLastMovement = TimeUtils.millis();
            }
        }
    }

    private float getSnakeHeadRotation() {
        switch (getSnakeMovement()) {
            case Movement.Left:
                return 180.0f;
            case Movement.None:
            case Movement.Right:
                return 0.0f;
            case Movement.Down:
                return 270.0f;
            case Movement.Up:
                return 90.0f;
            default:
                throw new IllegalStateException();
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        float delta = Gdx.graphics.getDeltaTime();

        //Draw background
        mBackground.draw(batch, 0, 0, getWidth(), getHeight());

        //Draw tiles
        for (int y = 0; y < COUNT_Y_TILES; y++) {
            for (int x = 0; x < COUNT_X_TILES; x++) {
                final TileData tile = getTile(x, y);
                float xPos = x * TILE_WIDTH;
                float yPos = y * TILE_HEIGHT;

                switch (tile.type) {
                    case Tile.Type.APPLE:
                        batch.draw(tile.color == TileData.COLOR_RED ? mAppleRed : mAppleGreen,
                                xPos, yPos, TILE_WIDTH, TILE_HEIGHT);
                        break;
                    case Tile.Type.SNAKE_START:
                        batch.draw(mSnakeStart, xPos, yPos, TILE_WIDTH_HALF, TILE_HEIGHT_HALF,
                                TILE_WIDTH, TILE_HEIGHT, 1.0f, 1.0f, getSnakeHeadRotation());

                        if (mGameOver) {
                            mCracklingAnimStateTime += delta;
                            final TextureRegion keyFrame = mCracklingAnimation.getKeyFrame(mCracklingAnimStateTime);
                            batch.draw(keyFrame, xPos, yPos, TILE_WIDTH_HALF, TILE_HEIGHT_HALF,
                                    TILE_WIDTH, TILE_HEIGHT, 1.0f, 1.0f, getSnakeHeadRotation());
                        }
                        break;
                    case Tile.Type.SNAKE_PART:
                        TextureAtlas.AtlasRegion texture = null;
                        if (tile.color == TileData.COLOR_GREEN || tile.color == TileData.COLOR_DEFAULT) {
                            texture = tile.extra == 0 ? mSnakePartG : mSnakePartGF;
                        } else if (tile.color == TileData.COLOR_RED) {
                            texture = tile.extra == 0 ? mSnakePartR : mSnakePartRF;
                        }
                        batch.draw(texture, xPos, yPos, TILE_WIDTH, TILE_HEIGHT);
                        break;
                }
            }
        }
    }
}
