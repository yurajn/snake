# Snake #
**written in Java, uses [libGDX 1.2.1](http://libgdx.badlogicgames.com/) engine**

tested on Android and Windows

### Used latest libGDX features ###
* AssetManager - effectively loading and managing resources (no static classes with still loaded resources in memory)
* TexturePacker (packs images into one texture - launched everytime on desktop) and TextureAtlas
* Strings localization (EN/SK - automatically choose default locale)
* Generating bitmap fonts from TTF files (+size is multiplied by DPI)
* ScreenAdapter, Scene2D (+simple logo animation), Dialog, custom Actor
* Simple animated menu background (uses Pool for effectively creating white moving stripes)
* Animation class
* other small things

### Directories ###
* **android**
    * contains only AndroidLauncher file
* **core**
    * contains main game code (main game logic is in World.java)
* **desktop**
    * contains only DesktopLauncher file (and contains TexturePacker too)

### Gameplay ###
On desktop use arrow keys, on android use vertical and horizontal touch

### Resources ###
All game resources are saved in */android/assets* directory

Resources are managed using AssetManager, paths to resources are in ResourcePaths.java

Sounds generated using [www.bfxr.net/](BFXR), graphics made by hand using [www.getpaint.net](Paint.NET)

### Resolution ###
Game screen uses FitViewport (virtual size 800x480) other ScreenViewport