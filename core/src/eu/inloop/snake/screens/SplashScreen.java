package eu.inloop.snake.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.I18NBundleLoader;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import eu.inloop.snake.Screen;
import eu.inloop.snake.Snake;
import eu.inloop.snake.common.FontUtils;
import eu.inloop.snake.common.ResourcePaths;

import java.util.Locale;

public class SplashScreen extends Screen {

    private final Stage mStage;
    private final Image mImgLogoP1, mImgLogoP2, mImgLoading;
    private boolean mAnimationFinished, mFontsGenerated;

    public SplashScreen(Snake gameInstance) {
        super(gameInstance);
        mStage = new Stage(new ScreenViewport());

        //Load logo assets
        mAssetManager.load(ResourcePaths.SplashScreen.TEXTURE_LOGO_P1, Texture.class);
        mAssetManager.load(ResourcePaths.SplashScreen.TEXTURE_LOGO_P2, Texture.class);
        mAssetManager.load(ResourcePaths.SplashScreen.TEXTURE_LOADING, Texture.class);
        mAssetManager.finishLoading();

        //Create stage actors
        mImgLogoP1 = new Image(mAssetManager.get(ResourcePaths.SplashScreen.TEXTURE_LOGO_P1, Texture.class));
        mImgLogoP2 = new Image(mAssetManager.get(ResourcePaths.SplashScreen.TEXTURE_LOGO_P2, Texture.class));
        mImgLoading = new Image(mAssetManager.get(ResourcePaths.SplashScreen.TEXTURE_LOADING, Texture.class));
        mImgLoading.setScaling(Scaling.none);
        mImgLoading.setVisible(false);
        mImgLoading.setX(mStage.getWidth() - mImgLoading.getWidth());

        setImagePart(mImgLogoP1);
        setImagePart(mImgLogoP2);

        mStage.addActor(mImgLogoP1);
        mStage.addActor(mImgLogoP2);
        mStage.addActor(mImgLoading);

        loadResources(mAssetManager);
    }

    private void loadResources(final AssetManager assetManager) {
        assetManager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(new InternalFileHandleResolver()));

        //Load fonts immediately
        assetManager.load(ResourcePaths.FONT_ROBOTO, FreeTypeFontGenerator.class);
        assetManager.finishLoading();

        //Generate TTF => Bitmap font in thread
        new Thread() {
            @Override
            public void run() {
                /*
                    There is probably better way of generating fonts in latest snapshot?
                    https://github.com/libgdx/libgdx/blob/master/tests/gdx-tests/src/com/badlogic/gdx/tests/FreeTypeFontLoaderTest.java
                 */
                FontUtils.preGenerateFonts(mAssetManager);
                mFontsGenerated = true;
            }

        }.run();

        //Load resources - atlas, strings, skin, sounds...
        assetManager.load(ResourcePaths.ATLAS_MAIN, TextureAtlas.class);
        assetManager.load(ResourcePaths.BUNDLE_STRINGS, I18NBundle.class, new I18NBundleLoader.I18NBundleParameter(Locale.getDefault()));
        assetManager.load(ResourcePaths.SKIN_MAIN, Skin.class, new SkinLoader.SkinParameter(ResourcePaths.ATLAS_MAIN));
        assetManager.load(ResourcePaths.Game.SOUND_PICKUP, Sound.class);
        assetManager.load(ResourcePaths.Game.SOUND_HIT, Sound.class);
    }

    private void setImagePart(final Image image) {
        image.setVisible(false);
        image.setFillParent(true);
        image.setScaling(Scaling.none);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mStage.act(delta);
        mStage.draw();

        //When resources loaded and splashscreen animation finished, change screen
        if (mAssetManager.update() && mFontsGenerated) {
            //Animation finished and all is loaded, we can go to next screen
            if (mAnimationFinished) {
                dispose();
                mGameInstance.setScreen(new MenuScreen(mGameInstance));
            } else {
                if (mImgLoading.isVisible()) {
                    mImgLoading.addAction(Actions.fadeOut(1.0f));
                }
            }
        } else {
            mImgLoading.setVisible(true);
        }
    }

    @Override
    public void resize(int width, int height) {
        mStage.getViewport().update(width, height, true);
    }

    @Override
    public void show() {
        runAnimation();
    }

    @Override
    public void dispose() {
        //Unload these - we will not use them anymore
        mAssetManager.unload(ResourcePaths.SplashScreen.TEXTURE_LOGO_P1);
        mAssetManager.unload(ResourcePaths.SplashScreen.TEXTURE_LOGO_P2);
        mAssetManager.unload(ResourcePaths.SplashScreen.TEXTURE_LOADING);
        mStage.dispose();
    }

    private void runAnimation() {
        //Logo animation
        mImgLogoP1.addAction(Actions.sequence(Actions.delay(0.5f),
                        Actions.parallel(Actions.visible(true), Actions.fadeOut(0.000001f), Actions.fadeIn(1.25f),
                                Actions.moveTo(-40.0f, 0), Actions.moveTo(0, 0, 1.0f, Interpolation.swing)), Actions.delay(1.75f),
                        Actions.fadeOut(0.25f))
        );

        mImgLogoP2.addAction(Actions.sequence(Actions.delay(0.5f),
                        Actions.parallel(Actions.show(), Actions.fadeOut(0.000001f), Actions.fadeIn(1.25f),
                                Actions.moveTo(40, 0), Actions.moveTo(0, 0, 1.0f, Interpolation.swing)), Actions.delay(1.75f),
                        Actions.fadeOut(0.25f), Actions.run(mNextScreenRunnable))
        );
    }

    private final Runnable mNextScreenRunnable = new Runnable() {
        @Override
        public void run() {
            mAnimationFinished = true;
        }
    };
}
