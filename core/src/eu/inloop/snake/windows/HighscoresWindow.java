package eu.inloop.snake.windows;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ScreenUtils;
import eu.inloop.snake.ScoreItem;
import eu.inloop.snake.common.Prefs;

import java.util.Arrays;

public class HighscoresWindow extends Window {

    public HighscoresWindow(Stage stage, Skin skin) {
        super("Highscores", skin);
        setMovable(false);
        setModal(true);

        char[] chars = new char[5];
        Arrays.fill(chars, ' ');
        String s = new String(chars);

        StringBuilder sb = new StringBuilder();
        Array<ScoreItem> scoreItems = Prefs.getScoreItems();
        if (scoreItems != null && scoreItems.size > 0) {
            for (int i = 0; i < Math.min(scoreItems.size, 10); i++) {
                ScoreItem item = scoreItems.get(i);

                sb.append(String.format("%d. %s%s%d\n", i + 1, item.name, s, item.score));
            }
        } else {
            sb.append("No highscores yet!");
        }
        Label scoresLabel = new Label(sb.toString(), skin);
        add(scoresLabel);
        pack();

        setPosition(Math.round((stage.getWidth() - getWidth()) / 2), Math.round((stage.getHeight() - getHeight()) / 2));
        addListener(new ActorGestureListener() {
            @Override
            public void tap(InputEvent event, float x, float y, int count, int button) {
                remove();
            }
        });
    }

    @Override
    public float getPrefWidth() {
        return 400;
    }

    @Override
    public float getPrefHeight() {
        return 400;
    }
}
