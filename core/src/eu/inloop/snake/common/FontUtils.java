package eu.inloop.snake.common;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectMap;

import java.util.Iterator;

public class FontUtils {

    private static ArrayMap<String, BitmapFont> sFonts = new ArrayMap<String, BitmapFont>();

    private FontUtils() { }

    public static void preGenerateFonts(AssetManager assetManager) {
        FontUtils.generateFont(assetManager, ResourcePaths.FONT_ROBOTO, 18);
        FontUtils.generateFont(assetManager, ResourcePaths.FONT_ROBOTO, 16);
    }

    public static BitmapFont generateFont(AssetManager assetManager, String font, int size) {
        String key = font + size;
        if (sFonts.containsKey(key)) {
            return sFonts.get(key);
        } else {
            final FreeTypeFontGenerator generator = assetManager.get(font, FreeTypeFontGenerator.class);
            final FreeTypeFontGenerator.FreeTypeFontParameter fontParameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            if (Gdx.app.getType() == Application.ApplicationType.Desktop) {
                fontParameter.size = Math.round(size * (Gdx.graphics.getDensity() * 2));
            } else {
                fontParameter.size = Math.round(size * Gdx.graphics.getDensity());
            }
            final BitmapFont generateFont = generator.generateFont(fontParameter);
            sFonts.put(key, generateFont);

            return generateFont;
        }
    }

    public static void unloadFont(String font, int size) {
        final BitmapFont value = sFonts.removeKey(font + size);
        if (value != null) {
            value.dispose();
        }
    }

    public static void disposeAll() {
        Iterator<ObjectMap.Entry<String, BitmapFont>> iterator = sFonts.iterator();
        while (iterator.hasNext()) {
            iterator.next().value.dispose();
            iterator.remove();
        }
    }
}
