package eu.inloop.snake.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import eu.inloop.snake.AnimatedBackground;
import eu.inloop.snake.Screen;
import eu.inloop.snake.Snake;
import eu.inloop.snake.common.FontUtils;
import eu.inloop.snake.common.Prefs;
import eu.inloop.snake.common.ResourcePaths;
import eu.inloop.snake.windows.HighscoresWindow;

public class MenuScreen extends Screen {

    private final Stage mStage;
    private final Skin mSkin;
    private Image mImgLogo;

    public MenuScreen(Snake gameInstance) {
        super(gameInstance);
        mStage = new Stage(new ScreenViewport());

        //Load assets
        final TextureAtlas textureAtlas = mAssetManager.get(ResourcePaths.ATLAS_MAIN, TextureAtlas.class);
        final I18NBundle lang = mAssetManager.get(ResourcePaths.BUNDLE_STRINGS, I18NBundle.class);
        mSkin = mAssetManager.get(ResourcePaths.SKIN_MAIN, Skin.class);

        //Generate fonts
        BitmapFont menuFont = FontUtils.generateFont(mAssetManager, ResourcePaths.FONT_ROBOTO, 18);
        mSkin.get("default", Window.WindowStyle.class).titleFont = FontUtils.generateFont(mAssetManager, ResourcePaths.FONT_ROBOTO, 16);
        mSkin.get("default", Label.LabelStyle.class).font = menuFont;
        mSkin.get(ResourcePaths.Menu.SKIN_BLUE_BTN, TextButton.TextButtonStyle.class).font = menuFont;
        mSkin.get(ResourcePaths.Menu.SKIN_VOLUME_BTN, CheckBox.CheckBoxStyle.class).font = menuFont;

        createMenu(textureAtlas, lang, mSkin);

        Gdx.input.setInputProcessor(mStage);
    }

    private void createMenu(TextureAtlas textureAtlas, I18NBundle lang, Skin skin) {
        Table backgroundTable = new Table();
        backgroundTable.setFillParent(true);
        backgroundTable.defaults().pad(8);
        backgroundTable.setBackground(new TiledDrawable(textureAtlas.findRegion(ResourcePaths.Menu.TEXTURE_BACKGROUND)));

        CheckBox chkVolume = new CheckBox(null, skin, ResourcePaths.Menu.SKIN_VOLUME_BTN);
        chkVolume.setChecked(Prefs.isSoundMuted());
        chkVolume.addListener(mMuteClickListener);
        backgroundTable.add(chkVolume).expandY().top();
        backgroundTable.add().expandX();

        Image imgLink = new Image(textureAtlas.findRegion(ResourcePaths.Menu.TEXTURE_LINK));
        imgLink.addListener(mLinkClickListener);
        backgroundTable.add(imgLink).top();

        mStage.addActor(backgroundTable);

        AnimatedBackground animatedBackground = new AnimatedBackground(mStage);
        mStage.addActor(animatedBackground);

        Table mainTable = new Table();
        mainTable.setFillParent(true);

        mImgLogo = new Image(textureAtlas.findRegion(ResourcePaths.Menu.TEXTURE_LOGO));
        mImgLogo.setOrigin(mImgLogo.getWidth() / 2, mImgLogo.getHeight() / 2);
        mImgLogo.setScaling(Scaling.none);

        mainTable.add(mImgLogo).top().padTop(32).padBottom(70);

        createMenuButton(skin, lang.get("menu_play"), mPlayClickListener, mainTable);
        createMenuButton(skin, lang.get("menu_highscore"), mHighScoreClickListener, mainTable);
        createMenuButton(skin, lang.get("menu_exit"), mExitClickListener, mainTable);

        mStage.addActor(mainTable);
    }

    private void createMenuButton(Skin skin, String text, ClickListener clickListener, Table parentTable) {
        final TextButton button = new TextButton(text, skin, ResourcePaths.Menu.SKIN_BLUE_BTN);
        button.pad(16, 0, 16, 0);
        button.addListener(clickListener);
        parentTable.row();
        parentTable.add(button).size(220, 60).padBottom(32);
    }

    private final ClickListener mPlayClickListener = new ClickListener() {
        @Override
        public void clicked(InputEvent event, float x, float y) {
            dispose();
            mGameInstance.setScreen(new GameScreen(mGameInstance));
        }
    };

    private final ClickListener mHighScoreClickListener = new ClickListener() {
        @Override
        public void clicked(InputEvent event, float x, float y) {
            HighscoresWindow window = new HighscoresWindow(mStage, mSkin);
            mStage.addActor(window);
        }
    };

    private final ClickListener mExitClickListener = new ClickListener() {
        @Override
        public void clicked(InputEvent event, float x, float y) {
            Gdx.app.exit();
        }
    };

    private final ClickListener mLinkClickListener = new ClickListener() {
        @Override
        public void clicked(InputEvent event, float x, float y) {
            Gdx.net.openURI("http://www.inloop.eu");
        }
    };

    private final ClickListener mMuteClickListener = new ClickListener() {
        @Override
        public void clicked(InputEvent event, float x, float y) {
            Prefs.setSoundMuted(((CheckBox) event.getListenerActor()).isChecked());
        }
    };

    @Override
    public void show() {
        //Logo animation
        mImgLogo.addAction(
                Actions.forever(
                        Actions.sequence(
                                Actions.scaleTo(1.1f, 1.1f, 0.5f, Interpolation.sineIn),
                                Actions.scaleTo(1.0f, 1.0f, 0.5f, Interpolation.sineOut)))
        );
    }

    @Override
    public void pause() {
        Prefs.saveSettings();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mStage.act(delta);
        mStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        mStage.getViewport().update(width, height, true);
    }

    @Override
    public void dispose() {
        Prefs.saveSettings();
        mStage.dispose();
    }
}
