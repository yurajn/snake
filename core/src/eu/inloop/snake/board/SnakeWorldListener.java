package eu.inloop.snake.board;

public interface SnakeWorldListener {
    public void onSnakeMove(int tileType);
    public void onGameOver();
    public void onGameStart();
}
