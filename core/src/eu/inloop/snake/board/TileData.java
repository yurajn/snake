package eu.inloop.snake.board;

public class TileData {

    public static final int COLOR_DEFAULT = 0;
    public static final int COLOR_RED = 1;
    public static final int COLOR_GREEN = 2;

    public int type;
    public int color;
    public int extra;

    public TileData(TileData data) {
        type = data.type;
        color = data.color;
        extra = data.extra;
    }

    public TileData(int type) {
        this.type = type;
        this.color = COLOR_DEFAULT;
    }

    public TileData(int type, int color) {
        this.type = type;
        this.color = color;
    }

    @Override
    public String toString() {
        return "TileData{" +
                "type=" + type +
                ", color=" + color +
                '}';
    }
}
