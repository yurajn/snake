package eu.inloop.snake.common;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.backends.lwjgl.audio.Ogg;

//This class does nothing special - only checks If sound is enabled
public class AudioUtils {

    private AudioUtils() { }

    public static void playSound(Sound sound, float volume, float pitch, float pan) {
        if (!Prefs.isSoundMuted()) {
            sound.play(volume, pitch, pan);
        }
    }

    public static void playSound(Sound sound, float volume) {
        if (!Prefs.isSoundMuted()) {
            sound.play(volume);
        }
    }

    public static void playSound(Sound sound) {
        if (!Prefs.isSoundMuted()) {
            sound.play();
        }
    }

}
