package eu.inloop.snake.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import eu.inloop.snake.ScoreItem;
import eu.inloop.snake.Screen;
import eu.inloop.snake.Snake;
import eu.inloop.snake.board.Movement;
import eu.inloop.snake.board.SnakeWorldListener;
import eu.inloop.snake.board.Tile;
import eu.inloop.snake.board.World;
import eu.inloop.snake.common.AudioUtils;
import eu.inloop.snake.common.Prefs;
import eu.inloop.snake.common.ResourcePaths;
import eu.inloop.snake.windows.ExitDialog;

public class GameScreen extends Screen {

    private final Stage mStage;
    private final Skin mSkin;
    private final I18NBundle mLang;
    private final TextureRegionDrawable mCtrlHorizontal, mCtrlVertical;
    private Label mScoreLabel, mGameOverLabel;
    private Image mMovementInfoIcon;
    private Container<Label> mScoreLabelWrapper;

    private Sound mPickupSound, mHitSound;
    private World mWorld;
    private int mTotalScore;
    private long mGameOverTimestamp;

    public GameScreen(Snake gameInstance) {
        super(gameInstance);
        mStage = new Stage(new FitViewport(Snake.VIRTUAL_WIDTH, Snake.VIRTUAL_HEIGHT));
        mSkin = mAssetManager.get(ResourcePaths.SKIN_MAIN, Skin.class);
        mLang = mAssetManager.get(ResourcePaths.BUNDLE_STRINGS, I18NBundle.class);
        mPickupSound = mAssetManager.get(ResourcePaths.Game.SOUND_PICKUP, Sound.class);
        mHitSound = mAssetManager.get(ResourcePaths.Game.SOUND_HIT, Sound.class);

        final TextureAtlas textureAtlas = mAssetManager.get(ResourcePaths.ATLAS_MAIN, TextureAtlas.class);
        mCtrlHorizontal = new TextureRegionDrawable(textureAtlas.findRegion(ResourcePaths.Game.TEXTURE_CTRL_HORIZONTAL));
        mCtrlVertical = new TextureRegionDrawable(textureAtlas.findRegion(ResourcePaths.Game.TEXTURE_CTRL_VERTICAL));

        mWorld = new World(mAssetManager, new MySnakeWorldListener());

        initUi();

        Gdx.input.setCatchBackKey(true);
        Gdx.input.setInputProcessor(mStage);

        mStage.addListener(new InputListener() {
            private ExitDialog exitDialog;

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                //Handle BACK/ESC - show exit dialog
                if ((exitDialog == null || !exitDialog.isVisible()) &&
                        (keycode == Input.Keys.BACK || keycode == Input.Keys.ESCAPE)) {
                    exitDialog = new ExitDialog(mAssetManager, mSkin, new Runnable() {
                        @Override
                        public void run() {
                            dispose();
                            mGameInstance.setScreen(new MenuScreen(mGameInstance));
                        }
                    }, new Runnable() {
                        @Override
                        public void run() {
                            mWorld.setPause(false);
                        }
                    });
                    exitDialog.show(mStage);
                    mWorld.setPause(true);
                }
                return super.keyUp(event, keycode);
            }

            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                //Support for keyboard control
                switch (keycode) {
                    case Input.Keys.LEFT:
                        mWorld.setSnakeMovement(Movement.Left);
                        break;
                    case Input.Keys.RIGHT:
                        mWorld.setSnakeMovement(Movement.Right);
                        break;
                    case Input.Keys.UP:
                        mWorld.setSnakeMovement(Movement.Up);
                        break;
                    case Input.Keys.DOWN:
                        mWorld.setSnakeMovement(Movement.Down);
                        break;
                }

                updateControlsStatusIcon();

                return super.keyDown(event, keycode);
            }
        });

        mWorld.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (mWorld.isGameOver()) {
                    //Small pause If game over so user can not accidentally start game
                    if (TimeUtils.timeSinceMillis(mGameOverTimestamp) >= 500) {
                        mWorld.reset();
                        mMovementInfoIcon.setDrawable(mCtrlHorizontal);
                        mGameOverTimestamp = 0;
                    }
                    return false;
                }

                //Touch control
                final int snakeMovement = mWorld.getSnakeMovement();
                if (snakeMovement == Movement.Left || snakeMovement == Movement.Right) {
                    if (y <= mStage.getHeight() / 2) {
                        mWorld.setSnakeMovement(Movement.Down);
                    } else {
                        mWorld.setSnakeMovement(Movement.Up);
                    }
                } else {
                    if (x <= mStage.getWidth() / 2) {
                        mWorld.setSnakeMovement(Movement.Left);
                    } else {
                        mWorld.setSnakeMovement(Movement.Right);
                    }
                }

                updateControlsStatusIcon();

                return super.touchDown(event, x, y, pointer, button);
            }
        });
    }

    private void updateControlsStatusIcon() {
        if (mWorld.getSnakeMovement() == Movement.Left || mWorld.getSnakeMovement() == Movement.Right) {
            mMovementInfoIcon.setDrawable(mCtrlVertical);
        } else {
            mMovementInfoIcon.setDrawable(mCtrlHorizontal);
        }

    }

    private void initUi() {
        Table mainTable = new Table();
        Table uiTable = new Table();
        Table boardTable = new Table();
        mainTable.setFillParent(true);
        mStage.addActor(mainTable);

        mGameOverLabel = new Label(mLang.get("game_over"), mSkin);
        mGameOverLabel.setColor(Color.RED);
        mGameOverLabel.setVisible(false);

        mMovementInfoIcon = new Image(mCtrlHorizontal);

        mScoreLabel = new Label("", mSkin);
        mScoreLabelWrapper = new Container<Label>(mScoreLabel);
        mScoreLabelWrapper.setTransform(true);
        updateScore(0);

        //Add top bar
        uiTable.add(mScoreLabelWrapper).left();
        uiTable.add().expandX();
        uiTable.add(mGameOverLabel).right();
        uiTable.add(mMovementInfoIcon).padLeft(8).right();
        mainTable.top().add(uiTable).pad(0, 8, 0, 8).height(40).expandX().fillX().row();

        //Add game board
        boardTable.add(mWorld).fill().expand();
        mainTable.add(boardTable).fill().expand();

        //mainTable.debug();
    }

    private final class MySnakeWorldListener implements SnakeWorldListener {
        @Override
        public void onSnakeMove(int tileType) {
            if (tileType == Tile.Type.APPLE) {
                updateScore(1);
                AudioUtils.playSound(mPickupSound, 1, 1, getSoundPan());
            }
        }

        @Override
        public void onGameOver() {
            Prefs.addScoreItem(new ScoreItem(mTotalScore));
            mGameOverTimestamp = TimeUtils.millis();
            mGameOverLabel.setVisible(true);
            AudioUtils.playSound(mHitSound, 1, 1, getSoundPan());
        }

        @Override
        public void onGameStart() {
            resetScore();
        }

        float getSoundPan() {
            float posRelative = ((float) mWorld.getSnakeHeadPosition().x + 1) / World.COUNT_X_TILES;
            return map(posRelative, 0.0f, 1.0f, -0.5f, 0.5f);
        }
    }

    private float map(float x, float inMin, float inMax, float outMin, float outMax) {
        return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }

    private void resetScore() {
        mGameOverLabel.setVisible(false);
        mTotalScore = 0;
        updateScore(0);
    }

    private void updateScore(int addScore) {
        mTotalScore += addScore;
        mScoreLabel.setText(mLang.format("score", mTotalScore));

        //Lets animate score label when incrementing
        if (addScore > 0) {
            mScoreLabelWrapper.setOrigin(mScoreLabel.getWidth() / 2, mScoreLabel.getHeight() / 2);
            mScoreLabelWrapper.addAction(Actions.sequence(Actions.scaleTo(1.15f, 1.15f, 0.1f), Actions.scaleTo(1.0f, 1.0f, 0.1f)));
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mStage.act(delta);
        mStage.draw();

        //Table.drawDebug(mStage);
    }

    @Override
    public void pause() {
        super.pause();
        mWorld.setPause(true);
    }

    @Override
    public void resume() {
        super.resume();
        mWorld.setPause(false);
    }

    @Override
    public void show() {
        mWorld.reset();
    }

    @Override
    public void resize(int width, int height) {
        mStage.getViewport().update(width, height, true);
    }

    @Override
    public void dispose() {
        mStage.dispose();
    }
}
