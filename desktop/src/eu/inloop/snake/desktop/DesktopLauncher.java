package eu.inloop.snake.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import eu.inloop.snake.Snake;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = Snake.VIRTUAL_WIDTH;
        config.height = Snake.VIRTUAL_HEIGHT;
        TexturePacker.processIfModified("images/unpacked", "images/", "textures");
		new LwjglApplication(new Snake(), config);
	}
}
