package eu.inloop.snake;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;

public abstract class Screen extends ScreenAdapter {
    //We need Game instance in every screen for changing screen etc.
    protected final Snake mGameInstance;
    protected final AssetManager mAssetManager;

    public Screen(final Snake gameInstance) {
        this.mGameInstance = gameInstance;
        this.mAssetManager = gameInstance.mAssetManager;
    }
}
