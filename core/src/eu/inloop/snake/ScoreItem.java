package eu.inloop.snake;

public class ScoreItem {
    public String name;
    public int score;

    public ScoreItem() {
    }

    public ScoreItem(String name, int score) {
        this.score = score;
        this.name = name;
    }

    public ScoreItem(int score) {
        this.score = score;
        this.name = "";
    }
}
