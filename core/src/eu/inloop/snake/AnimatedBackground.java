package eu.inloop.snake;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.TimeUtils;

public class AnimatedBackground extends Actor {

    private static class SpriteEx extends Sprite implements Pool.Poolable {
        public float speedX, speedY;

        private SpriteEx() { }
        @Override public void reset() { }
    }

    private final Array<SpriteEx> mActiveObjects;
    private final SpriteEx[] mLastObjectsInLine;
    private final Pool<SpriteEx> mSpritePool = Pools.get(SpriteEx.class);
    private final Stage mStage;
    private final Texture mTexture;
    private long mLastGenerationTime = 0;

    private static final int MAX_LINES = 5;

    public AnimatedBackground(final Stage stage) {
        mStage = stage;
        mActiveObjects = new Array<SpriteEx>();
        mLastObjectsInLine = new SpriteEx[MAX_LINES];

        //Generate texture
        Pixmap pixel = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixel.setColor(1.0f, 1.0f, 1.0f, 0.05f);
        pixel.fill();
        mTexture = new Texture(pixel);
        pixel.dispose();

        for (int i = 0; i < 4; i++) {
            generateLines(MAX_LINES);
        }
    }

    boolean pregenerated = false;

    private void generateLines(int count) {
        float pos = 0;
        for (int i = 0; i < count; i++) {
            SpriteEx line = mSpritePool.obtain();
            line.setTexture(mTexture);
            line.setSize(MathUtils.random(50, 350), MathUtils.random(15, 40));

            if (!pregenerated) {
                line.speedX = MathUtils.random(10, 60);
                line.setY(MathUtils.random(pos, pos + (line.getHeight() * 3)));
                pos = line.getY() + (line.getHeight() * 3);
                line.setX(mStage.getWidth() * 0.5f);
            } else {
                SpriteEx object = mLastObjectsInLine[i];
                line.setY(object.getY());
                line.speedX = object.speedX;
                line.setX(object.getX() - line.getWidth() - MathUtils.random(15, 50));
            }

            mLastObjectsInLine[i] = line;
            mActiveObjects.add(line);
        }
        mLastGenerationTime = TimeUtils.millis();
        pregenerated = true;
    }

    private Vector2 position = new Vector2();
    private Vector2 velocity = new Vector2();

    @Override
    public void draw(Batch batch, float parentAlpha) {
        final float deltaTime = Gdx.graphics.getDeltaTime();

        //TODO: find better way to generate lines
        if (TimeUtils.timeSinceMillis(mLastGenerationTime) > 3000 && mActiveObjects.size < 60) {
            generateLines(MAX_LINES);
        }

        for (SpriteEx object : mActiveObjects) {
            //update
            velocity.set(object.speedX, object.speedY).scl(deltaTime);
            position.set(object.getX(), object.getY());
            position.add(velocity);
            object.setPosition(position.x, position.y);

            if (object.getX() > mStage.getWidth()) {
                mSpritePool.free(object);
                mActiveObjects.removeValue(object, true);
                continue;
            }

            //render
            object.draw(batch, parentAlpha);
        }
    }
}
