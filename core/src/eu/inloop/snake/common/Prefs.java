package eu.inloop.snake.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Predicate;
import eu.inloop.snake.ScoreItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Prefs {

    private static final String NAME = "prefs";
    private static final String PREF_SOUND_MUTE = "PREF_SOUND_MUTE";
    private static final String PREF_SCORE = "PREF_SCORE";

    private static Preferences sPrefs;
    private static Array<ScoreItem> sPrecachedScoreItems;

    private Prefs() {
    }

    private static Preferences getPrefs() {
        if (sPrefs == null) {
            sPrefs = Gdx.app.getPreferences(NAME);
        }
        return sPrefs;
    }

    public static boolean isSoundMuted() {
        return getPrefs().getBoolean(PREF_SOUND_MUTE);
    }

    public static void setSoundMuted(boolean muted) {
        getPrefs().putBoolean(PREF_SOUND_MUTE, muted);
    }

    public static Array<ScoreItem> getScoreItems() {
        if (sPrecachedScoreItems == null) {
            Json json = new Json();
            final ScoreItem[] scoreItems = json.fromJson(ScoreItem[].class, getPrefs().getString(PREF_SCORE));

            if (scoreItems != null && scoreItems.length > 0) {
                sPrecachedScoreItems = new Array<ScoreItem>(scoreItems);

                //Lets sort items by score
                sPrecachedScoreItems.sort(new Comparator<ScoreItem>() {
                    @Override
                    public int compare(ScoreItem o1, ScoreItem o2) {
                        if (o2.score > o1.score) {
                            return 1;
                        } else if (o2.score < o1.score) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                });
            } else {
                sPrecachedScoreItems = new Array<ScoreItem>();
            }
        }

        return sPrecachedScoreItems;
    }

    public static void addScoreItem(ScoreItem scoreItem) {
        if (scoreItem.score == 0) {
            return;
        }

        final Array<ScoreItem> items = getScoreItems();

        //Check for score duplicate
        for (ScoreItem item : items) {
            if (item.score == scoreItem.score && item.name.equals(scoreItem.name)) {
                return;
            }
        }

        items.add(scoreItem);

        Json json = new Json();
        getPrefs().putString(PREF_SCORE, json.toJson(items.toArray()));
        sPrecachedScoreItems = null; //Score list has changed, remove cached version
    }

    public static void saveSettings() {
        getPrefs().flush();
    }
}
