package eu.inloop.snake.windows;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.I18NBundle;
import eu.inloop.snake.common.ResourcePaths;

public class ExitDialog extends Dialog {

    private final Runnable mOnPositiveClick, mOnNegativeClick;

    public ExitDialog(AssetManager assetManager, Skin skin, Runnable onPositiveClick, Runnable onNegativeClick) {
        super("", skin);
        mOnPositiveClick = onPositiveClick;
        mOnNegativeClick = onNegativeClick;

        final I18NBundle lang = assetManager.get(ResourcePaths.BUNDLE_STRINGS, I18NBundle.class);
        final TextButton.TextButtonStyle redButtonStyle = skin.get(ResourcePaths.Menu.SKIN_BLUE_BTN, TextButton.TextButtonStyle.class);

        pad(50);
        getButtonTable().padTop(30);
        getButtonTable().defaults().minWidth(120).minHeight(50);
        getButtonTable().defaults().space(70);

        text(lang.get("dialog_exit"));
        button(lang.get("yes"), true, redButtonStyle);
        button(lang.get("no"), null, redButtonStyle);
        setResizable(false);
        setMovable(false);
        setModal(true);
    }

    @Override
    protected void result(Object object) {
        if (object != null) {
            if (mOnPositiveClick != null) {
                mOnPositiveClick.run();
            } else {
                Gdx.app.exit();
            }
        } else {
            if (mOnNegativeClick != null) {
                mOnNegativeClick.run();
            }
        }
        setVisible(false);
    }
}
