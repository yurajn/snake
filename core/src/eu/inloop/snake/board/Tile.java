package eu.inloop.snake.board;

public class Tile {

    public static final class Type {
        public static final int OUTSIDE = -1;
        public static final int EMPTY = 0;
        public static final int APPLE = 1;
        public static final int SNAKE_START = 2;
        public static final int SNAKE_PART = 3;
    }

    public int x, y;
    public TileData data;

    public Tile(int x, int y) {
        this.x = x;
        this.y = y;
        this.data = new TileData(Type.EMPTY);
    }

    public Tile(int x, int y, TileData data) {
        this.x = x;
        this.y = y;
        this.data = data;
    }

    public Tile(Tile tile) {
        this.x = tile.x;
        this.y = tile.y;
        this.data = new TileData(Type.EMPTY);
    }

    public Tile(Tile tile, TileData data) {
        this.x = tile.x;
        this.y = tile.y;
        this.data = data;
    }

    @Override
    public String toString() {
        return "Tile{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
