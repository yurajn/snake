package eu.inloop.snake;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import eu.inloop.snake.common.FontUtils;
import eu.inloop.snake.screens.SplashScreen;

public class Snake extends Game {

    public static final int VIRTUAL_WIDTH = 800;
    public static final int VIRTUAL_HEIGHT = 480;

    protected final AssetManager mAssetManager = new AssetManager();

	@Override
	public void create () {
        //Set SplashScreen when game starts
        setScreen(new SplashScreen(this));
	}

    @Override
    public void dispose() {
        super.dispose();
        mAssetManager.dispose();
        FontUtils.disposeAll();
    }

}
