package eu.inloop.snake.common;

public final class ResourcePaths {
    private ResourcePaths() {
    }

    public static final String FONT_ROBOTO = "fonts/roboto.ttf";
    public static final String ATLAS_MAIN = "images/textures.atlas";
    public static final String SKIN_MAIN = "skin.json";
    public static final String BUNDLE_STRINGS = "strings/lang";

    public static final class SplashScreen {
        public static final String TEXTURE_LOGO_P1 = "images/inloop_logo_part1.png";
        public static final String TEXTURE_LOGO_P2 = "images/inloop_logo_part2.png";
        public static final String TEXTURE_LOADING = "images/loading.png";
    }

    public static final class Menu {
        public static final String TEXTURE_BACKGROUND = "menuBackground";
        public static final String TEXTURE_LOGO = "logo";
        public static final String TEXTURE_LINK = "link";

        public static final String SKIN_BLUE_BTN = "blueBtn";
        public static final String SKIN_VOLUME_BTN = "volumeBtn";
    }

    public static final class Game {
        public static final String TEXTURE_BACKGROUND = "grass";
        public static final String TEXTURE_SNAKE_START = "snakeStart";
        public static final String TEXTURE_SNAKE_PART_G = "snakePartGreen";
        public static final String TEXTURE_SNAKE_PART_GF = "snakePartGreenF";
        public static final String TEXTURE_SNAKE_PART_R = "snakePartRed";
        public static final String TEXTURE_SNAKE_PART_RF = "snakePartRedF";
        public static final String TEXTURE_APPLE_GREEN = "appleGreen";
        public static final String TEXTURE_APPLE_RED = "appleRed";
        public static final String TEXTURE_CTRL_VERTICAL = "ctrlVertical";
        public static final String TEXTURE_CTRL_HORIZONTAL = "ctrlHorizontal";

        public static final String SOUND_PICKUP = "sounds/pickup.ogg";
        public static final String SOUND_HIT = "sounds/hit.ogg";
        public static final String ANIMATION_CRACK = "animations/crack";
    }
}
